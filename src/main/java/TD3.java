int max2(int a, int b){
    // Action: Retourne le plus grand nombre parmis les 2
    if (a >= b){
        return a;
    }
    return b;
}

int max3(int a, int b, int c){
    // Action: Retourne le plus grand nombre parmis les 3 en utilisant max2()
    return max2(max2(a,b),c);
}

void testMax(){
    Ut.afficher("Entrez un premier nombre: ");
    int nb1 = Ut.saisirEntier();
    Ut.afficher("Entrez un second nombre: ");
    int nb2 = Ut.saisirEntier();
    Ut.afficherSL("Le nombre le plus grand est " + max2(nb1,nb2));
    Ut.afficher("Entrez un premier nombre: ");
    nb1 = Ut.saisirEntier();
    Ut.afficher("Entrez un deuxième nombre: ");
    nb2 = Ut.saisirEntier();
    Ut.afficher("Entrez un troisième nombre: ");
    int nb3 = Ut.saisirEntier();
    Ut.afficher("Le nombre le plus grand est " + max3(nb1,nb2,nb3));
}

void repeteCarac(int nb, char car){
    // Action: Affiche nb caractère 'car' à partir de la position
    for(int i = 1; i < (nb*2); i++){
        Ut.afficher(car);
    }
}

void pyramideSimple(int h, char c){
    // Action: Affiche une pyramide de heuteur h constitué de lignes répétant le caractère c
    for (int i = 1; i <= h; i++){
        for (int j = h; j > i; j--){
            Ut.afficher(" ");
        }
        repeteCarac(i,c);
        Ut.afficherSL("");
    }
}

void testPyramideSimple(){
    // Action: Demande à l'utilisateur une hauteur de pyramide et un caractère, puis affiche une pyramide
    Ut.afficher("Entrez la hauteur de la pyramide: ");
    int hauteur = Ut.saisirEntier();
    Ut.afficher("Entrez un caractère qui sera utilisé dans la pyramide: ");
    char car = Ut.saisirCaractere();
    pyramideSimple(hauteur, car);
}

void afficheNombresCroissante(int nb1, int nb2){
    // Action: Affiche les nombres croissants entre les deux nombres
    for (int i = 0; i <= nb2-nb1; i++){
        Ut.afficher((nb1+i)%10 + " ");
    }
}

void afficheNombresDecroissante(int nb1, int nb2){
    // Action: Affiche les nombres décroissants entre les deux nombres
    for (int i = 0; i <= nb2-nb1; i++){
        Ut.afficher((nb2-i)%10 + " ");
    }
}

void pyramideElaboree(int h){
    // Action: Créer la pyramide de chiffres
    for (int i = 1; i <= h; i++){
        repeteCarac((h-i+1),' ');
        afficheNombresCroissante(i,(2*i)-1);
        afficheNombresDecroissante(i,(2*i)-2);
        Ut.sautLigne();
    }
}

void testPyramideElaboree(){
    // Action: Affiche la pyramide de hauteur h
    int h = 0;
    while (h < 2){
        Ut.afficher("Entrez la hauteur de la pyramide: ");
        h = Ut.saisirEntier();
    }
    pyramideElaboree(h);
}

int nbCiffres(int n){
    // Action: retourne le nombre de chiffre de n
    int nbChiffres = 0;
    while (n > 0){
        n = n / 10;
        nbChiffres ++;
    }
    return nbChiffres;
}

int nbChiffresDuCarre(int n){
    // Action: retourne le nombre de chiffre de n au carré
    return nbCiffres(n*n);

}

int sommeDiviseur(int n){
    int score = 0;
    for (int i = 1; i < n; i++){
        if (n % i == 0){
            score += i;
        }
    }
    return score;
}

boolean amiEntreNombres(int q, int p){
    // Action: Verifie si p et q sont amis
    return((sommeDiviseur(q) == p) && (sommeDiviseur(p) == q));
}

void nbrAmis(){
    // Action: Affiche tout les amis qui sont inférieur ou égale à q et p
    int q = 0;
    int p = 0;
    while (q < 1){
        Ut.afficher("Veuillez entrer un nombre (qui doit être supérieur ou égale à 1): ");
        q = Ut.saisirEntier();
    }
    while (p < 1){
        Ut.afficher("Veuillez entrer un deuxième nombre (qui doit être supérieur ou égale à 1): ");
        p = Ut.saisirEntier();
    }
    for (int i = 1; i <= q; i++){
        for (int j = 1; j <= p; j++){
            if(amiEntreNombres(i,j)){
                Ut.afficherSL("Les nombres " + i + " et "+ j + " sont amis");
            }
        }
    }
    Ut.afficher("Fin...");
}

void nbrAmisBis(){
    // Action: Affiche tout les amis qui sont inférieur ou égale à q et p
    int n = 0;
    while (n < 1){
        Ut.afficher("Veuillez entrer un nombre (qui doit être supérieur ou égale à 1): ");
        n = Ut.saisirEntier();
    }
    for (int i = 1; i <= n; i++){
        for (int j = 1; j <= n; j++){
            if(amiEntreNombres(i,j)){
                Ut.afficherSL("Les nombres " + i + " et "+ j + " sont amis");
            }
        }
    }
    Ut.afficher("Fin...");
}

int racineParfaite(int c){
    // Action: Retourne la racinne carré de c du nombre entier
    int racine = (int) Math.sqrt(c);
    return ( c == racine*racine ? racine : -1);
}

boolean estCarreParfait(int nb){
    return ((racineParfaite(nb) != -1) ? true : false);
}

boolean  estTriangleRectangle (int c1, int c2){
    int hypotenuseCarre = (int)Math.pow(c1,2) + (int)Math.pow(c2,2);
    return (estCarreParfait(hypotenuseCarre) ? true : false);
}

int nbrTriangleRectangle(int n){
    int score = 0;
    for (int i = 1; i < n; i++){
        for (int j = 1; j < n; j++){
            for (int k = 1; k < n; k++){
                if (i+j+k <= n){
                    score ++;
                }
            }
        }
    }
    return (score);
}

boolean estSyracusien(int n){
    if (n % 2 == 0){
        n = n / 2;
    } else {
        n = (n * 3) + 1;
    }
    return ((n == 1)? true : false);
}

boolean estSyracusienAvecMaxOp(int n, int maxOps){
    // Action: Vérifie si le nombre n est syracusien avec un nombre maximum d'essais
    for (int i = 1; i <= maxOps ; i++) {
        if (n == 1) {
            return true;
        } else if (n % 2 == 0){
            n = n / 2;
        } else {
            n = (n*3) +1;
        }
    }
    return estSyracusien(n);
}

public void main() {
    for (int i = 1; i <= 10; i++) {
        Ut.afficherSL(estSyracusienAvecMaxOp(i,250));
    }
    nbrAmisBis();
}